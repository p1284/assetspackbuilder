using System;
using System.Collections.Generic;
using AssetPackBuilder.Config;
using GameReady.EditorTools;
using UnityEditor;
using UnityEngine;

namespace AssetPackBuilder.Editor
{
    [CustomEditor(typeof(AssetPacksConfig))]
    public class AssetPackBuilderEditor : UnityEditor.Editor
    {
        [MenuItem("Tools/AssetsPackBuilder/Create Or Select Config")]
        public static void CreateAssetPackBuilder()
        {
            EditorHelpers.CreateScriptableAsset<AssetPacksConfig>(true, true);
        }

        private AssetPacksConfig _config;

        private void OnEnable()
        {
            _config = (AssetPacksConfig)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Validate"))
            {
                ValidateAssetPacks();
            }

            if (GUILayout.Button("Show Selected Assets"))
            {
                foreach (var assetPack in _config.assets)
                {
                    if (!assetPack.selected) continue;
                    foreach (var asset in assetPack.assets)
                    {
                        var path = AssetDatabase.GetAssetPath(asset);
                        if (path.Contains("~"))
                        {
                            var originPath = path;
                            var index = path.IndexOf("~", StringComparison.Ordinal);
                            path = path.Remove(index, 1);
                            AssetDatabase.MoveAsset(originPath, path);
                            AssetDatabase.Refresh();
                        }
                    }
                }
            }

            if (GUILayout.Button("Hide Selected Assets"))
            {
                foreach (var assetPack in _config.assets)
                {
                    if (!assetPack.selected) continue;

                    foreach (var asset in assetPack.assets)
                    {
                        var path = AssetDatabase.GetAssetPath(asset);
                        if (path.Contains("~"))
                        {
                            continue;
                        }

                        AssetDatabase.MoveAsset(path, path + "~");
                        AssetDatabase.Refresh();
                    }
                }
            }

            if (GUILayout.Button("Export all"))
            {
                if (!ValidateAssetPacks()) return;

                foreach (var assetPack in _config.assets)
                {
                    var exportTarget = assetPack.exportTarget == null
                        ? _config.exportTargetProject
                        : assetPack.exportTarget;
                    var path = AssetDatabase.GetAssetPath(exportTarget);
                    AssetDatabase.ExportPackage(GetAssetPaths(assetPack),
                        path + "/" + assetPack.assetPackName + ".unitypackage",
                        assetPack.Options);
                }

                EditorUtility.SetDirty(_config);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            if (GUILayout.Button("Export Selected"))
            {
                if (!ValidateAssetPacks()) return;

                foreach (var assetPack in _config.assets)
                {
                    if (assetPack.selected)
                    {
                        assetPack.selected = false;
                        var exportTarget = assetPack.exportTarget == null
                            ? _config.exportTargetProject
                            : assetPack.exportTarget;
                        var path = AssetDatabase.GetAssetPath(exportTarget);
                        AssetDatabase.ExportPackage(GetAssetPaths(assetPack),
                            path + "/" + assetPack.assetPackName + ".unitypackage",
                            assetPack.Options);
                    }
                }

                EditorUtility.SetDirty(_config);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        private string[] GetAssetPaths(AssetPack pack)
        {
            var list = new List<string>();

            foreach (var packAsset in pack.assets)
            {
                list.Add(AssetDatabase.GetAssetPath(packAsset));
            }

            return list.ToArray();
        }


        private bool ValidateAssetPacks()
        {
            Debug.ClearDeveloperConsole();
            foreach (var assetPack in _config.assets)
            {
                if (_config.exportTargetProject == null)
                {
                    if (assetPack.exportTarget == null)
                    {
                        Debug.LogError("AssetPack export folder is null");
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(assetPack.assetPackName))
                {
                    Debug.LogError("AssetPack assetPackName is null", _config);
                    return false;
                }


                if (assetPack.assets.Length == 0)
                {
                    Debug.LogError("AssetPack asset is empty", _config);
                    return false;
                }
            }

            return true;
        }
    }
}