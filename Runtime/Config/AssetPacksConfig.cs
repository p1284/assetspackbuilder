using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Object = UnityEngine.Object;

namespace AssetPackBuilder.Config
{
    [Serializable]
    public class AssetPack
    {
        public Object exportTarget;
        public Object[] assets;
        public string assetPackName;
        public bool selected;
#if UNITY_EDITOR
        public ExportPackageOptions Options = ExportPackageOptions.Recurse;
#endif
    }

    public class AssetPacksConfig : ScriptableObject
    {
        public Object exportTargetProject;
        public AssetPack[] assets;
    }
}